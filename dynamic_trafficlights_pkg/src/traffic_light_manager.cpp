// GAZEBO STUFF
#include <gazebo/gazebo.hh>
#include <gazebo/physics/World.hh>
#include <gazebo/physics/Link.hh>
#include <gazebo/physics/Model.hh>

// ROS STUFF
#include "std_msgs/Float32.h"
#include <std_srvs/Empty.h>
#include <ros/ros.h>
#include "ros/callback_queue.h"


namespace gazebo {
    class TrafficLightPlugin : public WorldPlugin {
        public:
        TrafficLightPlugin() : WorldPlugin() {
            printf("Plugin constructor method!\n");
        }

        public:
        void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf) {
            // Make sure the ROS node for Gazebo has already been initialized
            if (!ros::isInitialized()) {
                ROS_FATAL_STREAM(
                    "A ROS node for Gazebo has not been initialized, unable to load "
                    "plugin. "
                    << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in "
                        "the gazebo_ros package)");
                return;
            }

            this->world = _world;
            GZ_ASSERT(this->world != NULL, "Got NULL world pointer!");
            this->sdf = _sdf;
            GZ_ASSERT(this->sdf != NULL, "Got NULL SDF element pointer!");

            // Check if Config Elements exist, otherwise they will have default value
            if (_sdf->HasElement("update_frequency"))
            this->update_frequency = _sdf->Get<double>("update_frequency");

            std::string model_traffic_lights_list = "";
            char delimiter = ',';

            if (_sdf->HasElement("model_traffic_lights"))

            model_traffic_lights_list =
                _sdf->Get<std::string>("model_traffic_lights");

            this->model_traffic_lights =
                this->split(model_traffic_lights_list, delimiter);

            for (auto &model_name_x : this->model_traffic_lights) {
            ROS_DEBUG("Traffic Lights model name==%s", model_name_x.c_str());
            }
            // CONGIG PART END
            

            // Create the node
            this->node = transport::NodePtr(new transport::Node());
            this->node->Init("default");

            if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info) ) {
            ros::console::notifyLoggerLevelsChanged();
            }

            // Init Gazebo Publishers
            this->visPub =
                this->node->Advertise<gazebo::msgs::Visual>("/gazebo/default/visual");
            ROS_DEBUG("Waiting for connection of Visual Topic...");
            this->visPub->WaitForConnection();
            ROS_DEBUG("Waiting for connection of Visual Topic...DONE");

            this->lightPub = this->node->Advertise<gazebo::msgs::Light>(
                "/gazebo/default/light/modify");
            ROS_DEBUG("Waiting for connection of Light Topic...");
            this->lightPub->WaitForConnection();
            ROS_DEBUG("Waiting for connection of Light Topic...DONE");

            // We wait for all system to be ready an amount of seconds
            float seconds_to_wait = 5.0;
            this->WaitForseconds(seconds_to_wait);

            // Update Time Init
            this->old_secs = this->world->SimTime().Float();
            // Listen to the update event. This event is broadcast every
            // simulation iteration.
            this->updateConnection = event::Events::ConnectWorldUpdateBegin(
                std::bind(&TrafficLightPlugin::OnUpdate, this));

            // ROS Stuff Now
            // Create our ROS node. This acts in a similar manner to
            // the Gazebo node
            this->rosNode.reset(new ros::NodeHandle("earthquake_rosnode"));

            serviceName = "/traffic_lights_service";
            // advertise services for calibration and bias setting
            if (!serviceName.empty())
            srv_ = this->rosNode->advertiseService(
                serviceName, &TrafficLightPlugin::ServiceCallback, this);
            // start custom queue for imu
            callback_queue_thread_ = boost::thread(
                boost::bind(&TrafficLightPlugin::CallbackQueueThread, this));

            this->InitTrafficLightStates();

            ROS_DEBUG("Traffic Lights Plugin Initialised and ready.........");
        }


        std::vector<std::string> split(const std::string &s, char delimiter) {
            std::vector<std::string> tokens;
            std::string token;
            std::istringstream tokenStream(s);
            while (std::getline(tokenStream, token, delimiter)) {
            tokens.push_back(token);
            }
            return tokens;
        }


        void WaitForseconds(float seconds_to_wait) {
            unsigned int microseconds;
            microseconds = seconds_to_wait * 1e6;
            ROS_DEBUG("Waiting for %f seconds", seconds_to_wait);
            usleep(microseconds);
            ROS_DEBUG("Done waiting...");
        }

        void OnUpdate() {

            if (this->reseting_plugin) {
            ROS_DEBUG("Reseting in Process, please wait...");
            } else {
            // TODO: Check what is necessary now here
            double new_secs = this->world->SimTime().Float();
            double delta = new_secs - this->old_secs;

            double max_delta = 0.0;

            if (this->update_frequency != 0.0) {
                max_delta = 1.0 / this->update_frequency;
            }

            if (delta > max_delta && delta != 0.0) {
                // We update the Old Time variable.
                this->old_secs = new_secs;
            }
            }
        }

        bool ServiceCallback(std_srvs::Empty::Request &req,
                       std_srvs::Empty::Response &res) {
            boost::mutex::scoped_lock scoped_lock(lock);
            ROS_INFO("You Called The change TrafficLights Service!"); 

            this->ChangeTrafficLights();           

            return true;
        }

        void CallbackQueueThread() {
            static const double timeout = 0.01;

            while (this->rosNode->ok()) {
            this->callback_queue_.callAvailable(ros::WallDuration(timeout));
            }
        }

        void InitTrafficLightStates() {
            // We have to clear the Maps otherwise it wont reflect the changes
            // ROS_DEBUG("Clearing modelIDToName MAP...");
            this->trafficLightsStates.clear();
            // this->OutputWorldModelsData();
            // ROS_DEBUG("Clearing modelIDToName MAP...DONE");

            // Initialize color map.
            for (auto model : this->world->Models()) {  

                if (model->GetName().find("traffic_light") != std::string::npos) {
                    int random_state = 1 + (rand() % 100);
                    ROS_INFO("================>>>> random_state=%i", random_state);
                    ROS_INFO("Model %s ", model->GetName().c_str());
                    if (random_state > 50){
                        ROS_INFO("INIT VALUE GREEN");
                        this->trafficLightsStates[model->GetName()] = "GREEN";
                    }else{
                        ROS_INFO("INIT VALUE RED");
                        this->trafficLightsStates[model->GetName()] = "RED";
                    }
                    
                }
                
            }
        }

        void ToggleTrafficLightState(std::string taffic_light_name) {

            if (taffic_light_name.find("traffic_light") != std::string::npos)
            {
                
                if (this->trafficLightsStates[taffic_light_name] == "GREEN") {
                    ROS_DEBUG("Model %s Toggle RED", taffic_light_name.c_str());
                    this->trafficLightsStates[taffic_light_name] = "YELLOW";
                } else if (this->trafficLightsStates[taffic_light_name] == "YELLOW") {
                    ROS_DEBUG("Model %s Toggle RED", taffic_light_name.c_str());
                    this->trafficLightsStates[taffic_light_name] = "RED";
                } else {
                    ROS_DEBUG("Model %s Toggle RED", taffic_light_name.c_str());
                    this->trafficLightsStates[taffic_light_name] = "GREEN";
                }

                bool result = this->ChangeModelColourTrafficLight(taffic_light_name, this->trafficLightsStates[taffic_light_name]); 
                    
            }
            
        }

        void UpdateWorldModels() {

            this->modelIDToName.clear();

            // Initialize color map.
            for (auto model : this->world->Models()) {                
                this->modelIDToName[model->GetId()] = model->GetName();
            }
        }

        void OutputWorldModelsData() {
            ROS_DEBUG("Start OutputWorldModelsData...");

            for (auto const &x : this->modelIDToName) {
            ROS_DEBUG("ModelID=%i, Name=%s", x.first, x.second.c_str());
            }

            ROS_DEBUG("END OutputWorldModelsData...");
        }

        void ChangeTrafficLights() {
            this->UpdateWorldModels();
            this->OutputWorldModelsData();

            // Toggle Traffilights colours
            for (auto const &x : this->modelIDToName) {
                ROS_DEBUG("ModelID=%i, Name=%s", x.first, x.second.c_str());
                std::string model_name = x.second;                
                this->ToggleTrafficLightState(model_name); 
            }
            
        }

        bool ChangeModelColourTrafficLight(std::string model_name, std::string trafic_light_state) {

            // INIT COLOURS 
            int new_R = 0;
            int new_G = 0;
            int new_B = 0;
            int new_ALFA = 255;

            // ROS_DEBUG("START ChangeModelColour...");
            gazebo::physics::ModelPtr model = this->world->ModelByName(model_name);

            if (!model) {
                ROS_ERROR("Model named [%s] could not be found", model_name.c_str());
                return false;
            } else {
                ROS_DEBUG("Model %s Found in World", model_name.c_str());
            }

            

            // ROS_DEBUG("Created ColorMsg and diffuseMsg");

            for (auto link : model->GetLinks()) {
            // Get all the visuals
            sdf::ElementPtr linkSDF = link->GetSDF();

            if (!linkSDF) {
                ROS_ERROR("Link had NULL SDF");
                return false;
            } else {
                ROS_DEBUG("Link [%s] SDF found", link->GetName().c_str());
            }

            if (linkSDF->HasElement("visual")) {

                for (sdf::ElementPtr visualSDF = linkSDF->GetElement("visual");
                    visualSDF; visualSDF = linkSDF->GetNextElement("visual")) 
                {
                    ROS_DEBUG("Processing Each Visual Element of the Link...");

                    if (visualSDF->HasAttribute("name")) {
                        ROS_DEBUG("visualSDF HAS the attribute name");
                    } else {
                        ROS_ERROR("attribute name in visualSDF NOT FOUND");
                    }

                    // GZ_ASSERT(visualSDF->HasAttribute("name"), "Malformed visual
                    // element!");

                    std::string visualName = visualSDF->Get<std::string>("name");

                    gazebo::msgs::Visual visMsg;

                    visMsg = link->GetVisualMessage(visualName);

                    ROS_DEBUG("visualName=%s", visualName.c_str());
                    ROS_DEBUG("link->GetScopedName()=%s", link->GetScopedName().c_str());

                    char delimiter = ':';
                    this->model_link_v = this->split(link->GetScopedName(), delimiter);

                    for (auto &model_name : this->model_link_v) {
                        ROS_DEBUG("Traffic Lights LINK==%s", model_name.c_str());
                    }

                    ROS_DEBUG("Link %s Name", this->model_link_v[2].c_str());
                    ROS_DEBUG("trafic_light_state==%s", trafic_light_state.c_str());

                    bool have_to_change_colour = false;

                    if (this->model_link_v[2] == "green_light") {
                        if (trafic_light_state == "GREEN"){
                            ROS_DEBUG("GREEN LIGHT SET==%s", this->model_link_v[2].c_str());
                            new_R = 0;
                            new_G = 255;
                            new_B = 0;
                        }else{
                            ROS_DEBUG("BLACK LIGHT SET==%s", this->model_link_v[2].c_str());
                            new_R = 0;
                            new_G = 0;
                            new_B = 0;
                        }
                        have_to_change_colour = true;
                    }else if (this->model_link_v[2] == "yellow_light") {
                        if (trafic_light_state == "YELLOW"){
                            ROS_DEBUG("YELLOW LIGHT SET==%s", this->model_link_v[2].c_str());
                            new_R = 255;
                            new_G = 255;
                            new_B = 0;
                        }else{
                            ROS_DEBUG("BLACK LIGHT SET==%s", this->model_link_v[2].c_str());
                            new_R = 0;
                            new_G = 0;
                            new_B = 0;
                        }
                        have_to_change_colour = true;
                    }else if (this->model_link_v[2] == "red_light") {
                        if (trafic_light_state == "RED"){
                            ROS_DEBUG("RED LIGHT SET==%s", this->model_link_v[2].c_str());
                            new_R = 255;
                            new_G = 0;
                            new_B = 0;
                        }else{
                            ROS_DEBUG("BLACK LIGHT SET==%s", this->model_link_v[2].c_str());
                            new_R = 0;
                            new_G = 0;
                            new_B = 0;
                        }
                        have_to_change_colour = true;
                    } else {
                        ROS_DEBUG("LINK NOT A TRAFFIC LIGHT");
                    }

                    if (have_to_change_colour){
                        // Set and publish colours
                        ROS_DEBUG("Setting Colours");

                        ROS_DEBUG("RGB-RANDOM=[%i,%i,%i,%i], model=%s", new_R, new_G, new_B,
                    new_ALFA, model_name.c_str());
                        ignition::math::Color newColor(new_R, new_G, new_B, new_ALFA);
                        ignition::math::Color newColorE(new_R, new_G, new_B, new_ALFA);
                        ignition::math::Color newColorD(new_R, new_G, new_B, new_ALFA);

                        gazebo::msgs::Color *colorMsg = new gazebo::msgs::Color(gazebo::msgs::Convert(newColor));
                        gazebo::msgs::Color *emissivecolorMsg = new gazebo::msgs::Color(gazebo::msgs::Convert(newColorE));
                        gazebo::msgs::Color *diffuseMsg = new gazebo::msgs::Color(gazebo::msgs::Convert(newColorD));

                        if ((!visMsg.has_material()) || visMsg.mutable_material() == NULL) {
                            gazebo::msgs::Material *materialMsg = new gazebo::msgs::Material;
                            visMsg.set_allocated_material(materialMsg);
                        } else {
                            ROS_ERROR("visMsg doesnt have material or mutable_material");
                        }

                        gazebo::msgs::Material *materialMsg = visMsg.mutable_material();

                        ROS_DEBUG("materialMsg Created");
                        visMsg.set_name(link->GetScopedName());
                        visMsg.set_parent_name(model->GetScopedName());
                        
                        materialMsg->set_allocated_ambient(colorMsg);
                        materialMsg->set_allocated_diffuse(diffuseMsg);
                        materialMsg->set_allocated_emissive(emissivecolorMsg);

                        ROS_DEBUG("visMsg READY TO PUBLISH EMISSIVE");
                        this->visPub->Publish(visMsg);
                        ROS_DEBUG("visMsg PUBLISH DONE EMISSIVE");
                    }
                    

                    
                }

            } else {
                ROS_ERROR("Link has no Element visual");
            }
            }

            ROS_DEBUG("END ChangeModelColour...");
            return true;
        }

        
        // Variables
        // Pointer to the update event connection
        private:
        event::ConnectionPtr updateConnection;

        /// \brief World pointer.
        protected:
        gazebo::physics::WorldPtr world;
        /// \brief SDF pointer.
        protected:
        sdf::ElementPtr sdf;
        /// \brief Maps model IDs to colors
        private:
        /// \brief Maps model IDs to ModelNames
        private:
        std::map<int, std::string> modelIDToName;
        private:
        std::map<std::string, std::string> trafficLightsStates;

        private:
        std::map<int, std::string> lightIDToName;

        // Time Memory
        double old_secs;
        // Frequency of update
        double update_frequency = 1.0;        

        std::vector<std::string> model_traffic_lights;
        std::vector<std::string> model_link_v;
        
        // Reseting Flag
        bool reseting_plugin = false;

        /// \brief A node used for transport
        private:
        transport::NodePtr node;
        /// \brief For publishing visual messages to ~/visual
        private:
        transport::PublisherPtr visPub;
        // Publisher for light modification
        transport::PublisherPtr lightPub;

        // ROS Stuff
        /// \brief A node use for ROS transport
        private:
        std::unique_ptr<ros::NodeHandle> rosNode;

        private:
        ros::ServiceServer srv_;

        private:
        ros::CallbackQueue callback_queue_;

        private:
        boost::thread callback_queue_thread_;

        private:
        std::string serviceName;
        /// \brief A mutex to lock access to fields that are used in message callbacks
        boost::mutex lock;


    };

    // Register plugin
    GZ_REGISTER_WORLD_PLUGIN(TrafficLightPlugin)
}